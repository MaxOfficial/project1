<!DOCTYPE html>
<html>
    <head>
       <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}">
        <title>Pluridisciplinary by Bond University</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/colors.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="style/style.css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->
        <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
        <script src="js/script.js"></script>
    </head>
    <body>
    <div id="particles-js">   
    </div>
    <div class="container">
        <form action="upload/upload.php">
            <div class="content">
                <div class="title">tap or drag document here to upload</div>
                <div class="add-file">
                <label class="file_upload">
                    <span>+</span>
                    <input type="file" name="file" class="file-input">
                </label>
                </div>
                <div class="footer-title">accepts .pdf &amp; .docs</div>
            </div>
        </form>
    </div> 
    <!-- scripts -->
    <script src="particlesjs/particles.js"></script>
    <script src="particlesjs/app.js"></script>

    </body>
</html>
