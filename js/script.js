$(document).ready(function() {
    
    var dropZone = $('.content');
    
    // Проверка поддержки браузером
    if (typeof(window.FileReader) == 'undefined') {
        dropZone.text('Не поддерживается браузером!');
        dropZone.addClass('error');
    }
    
    // Добавляем класс hover при наведении
    dropZone[0].ondragover = function() {
        dropZone.addClass('hover');
        return false;
    };
    
    // Убираем класс hover
    dropZone[0].ondragleave = function() {
        dropZone.removeClass('hover');
        return false;
    };
    
    // Обрабатываем событие Drop
    dropZone.on("drop",function(event) {
        event.preventDefault();
        var file = event.dataTransfer.files[0];
        if(file.name.split(".")[file.name.split(".").length-1].toLowerCase() == "pdf" || file.name.split(".")[file.name.split(".").length-1].toLowerCase() == "docs"){
            dropZone.removeClass('hover');
            dropZone.addClass('drop');


            // Создаем запрос
            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener('progress', uploadProgress, false);
            xhr.onreadystatechange = stateChange;
            xhr.open('POST', 'php/upload.php');
            xhr.setRequestHeader('X-FILE-NAME', file.name);
            xhr.send(file);
        }else{
            dropZone.removeClass('hover');
            console.log("Ошибка");
            return false;           
        }
    });    
    // Показываем процент загрузки
    function uploadProgress(event) {
        var percent = parseInt(event.loaded / event.total * 100);
        dropZone.text('Загрузка: ' + percent + '%');
    }
    
    // Пост обрабочик
    function stateChange(event) {
        if (event.target.readyState == 4) {
            if (event.target.status == 200) {
                dropZone.text('Загрузка успешно завершена!');
            } else {
                dropZone.text('Произошла ошибка!');
                dropZone.addClass('error');
            }
        }
    }

    $(".file-input").on("change",function(){
        if($(this).val().split(".")[$(this).val().split(".").length-1].toLowerCase() == "pdf" || $(this).val().split(".")[$(this).val().split(".").length-1].toLowerCase() == "docs"){
            // Создаем запрос
            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener('progress', uploadProgress, false);
            xhr.onreadystatechange = stateChange;
            xhr.open('POST', 'php/upload.php');
            xhr.setRequestHeader('X-FILE-NAME', $(this).val());
            xhr.send($(this).val());
            
        }else{
            console.log("Ошибка");
            return false;           
        }
        
    });
    
});